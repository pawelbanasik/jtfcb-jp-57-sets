import java.util.HashSet;
import java.util.Set;

public class Main {

	public static void main(String[] args) {

		// Set (zbiory) is collection that stores unique elements

		// HashSet nie zachowuje kolejnosci
		// Set <String> set1 = new HashSet<String> ();

		// Set <String> set1 = new LinkedHashSet<String> ();
		Set<String> set1 = new HashSet<String>();

		if (set1.isEmpty()) {
			System.out.println("set is empty");

		}

		set1.add("dog");
		set1.add("cat");
		set1.add("mouse");
		set1.add("snake");
		set1.add("bear");

		if (set1.isEmpty()) {
			System.out.println("set is not empty");

		}

		// adding duplicate items does nothing
		set1.add("mouse");
		System.out.println(set1);

		//////////// Iteration//////////////
		for (String element : set1) {

			System.out.println(element);

		}
		////////// does set contain a given item?
		if (set1.contains("ardvak")) {
			System.out.println("contains aardvak");
		}
		if (set1.contains("cat")) {
			System.out.println("contains cat");

		}

		Set<String> set2 = new HashSet<String>();

		set2.add("dog");
		set2.add("cat");
		set2.add("giraffe");
		set2.add("monkey");
		set2.add("ant");
		
		// czesc wspolna
		Set <String> intersection = new HashSet<String>(set1);
		System.out.println(intersection);
		
		intersection.retainAll(set2);
		System.out.println(intersection);
		
		
		// roznica w zbiorach
		Set<String> difference = new HashSet<String>(set2);
		difference.removeAll(set1);
		System.out.println(difference);
	}

}
